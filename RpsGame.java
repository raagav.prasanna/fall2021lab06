// Raagav Prasanna 2036159
import java.util.Random;
public class RpsGame {
    private int noWins;
    private int noTies;
    private int noLoss;
    private Random randNumGen;
    public int getWins() {
        return this.noWins;
    }
    public int getTies() {
        return this.noTies;
    }
    public int getLoss() {
        return this.noLoss;
    }
    public String playRound(String choice) {
        final int rockInd = 0; final int paperInd = 1; final int scissorsInd = 2;
        String retStr = "";
        String[] choiceArr = new String[]{"rock","paper","scissors"};
        boolean exists = false;
        this.randNumGen = new Random();
        String compChoice = choiceArr[randNumGen.nextInt(choiceArr.length)];
        for (String string : choiceArr) {
            if(string.equals(choice)) {
                exists = true;
            }
        }
        if(!exists) {
            retStr = "the input selection is not a proper choice";
        }
        else if(choice.equals(compChoice)) {
            this.noTies++;
            retStr = "Tie, Computer chose " +compChoice +" and Player chose " +choice;
        }
        else if(choice.equals(choiceArr[rockInd]) && compChoice.equals(choiceArr[scissorsInd])) {
            this.noWins++;
            retStr = ("Player Wins! Player chose " +choice +" and Computer choose " +compChoice);
        }
        else if(choice.equals(choiceArr[paperInd]) && compChoice.equals(choiceArr[rockInd])) {
            this.noWins++;
            retStr = ("Player Wins! Player chose " +choice +" and Computer choose " +compChoice);
        }
        else if(choice.equals(choiceArr[scissorsInd]) && compChoice.equals(choiceArr[paperInd])) {
            this.noWins++;
            retStr = ("Player Wins! Player chose " +choice +" and Computer choose " +compChoice);
        }
        else if(compChoice.equals(choiceArr[rockInd]) && choice.equals(choiceArr[scissorsInd])) {
            this.noLoss++;
            retStr = ("Computer Wins! Player chose " +choice +" and Computer choose " +compChoice);
        }
        else if(compChoice.equals(choiceArr[paperInd]) && choice.equals(choiceArr[rockInd])) {
            this.noLoss++;
            retStr = ("Computer Wins! Player chose " +choice +" and Computer choose " +compChoice);
        }
        else if(compChoice.equals(choiceArr[scissorsInd]) && choice.equals(choiceArr[paperInd])) {
            this.noLoss++;
            retStr = ("Computer Wins! Player chose " +choice +" and Computer choose " +compChoice);
        }
        return retStr;
    }
}