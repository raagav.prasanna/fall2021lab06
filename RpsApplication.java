// Raagav Prasanna 2036159
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class RpsApplication extends Application {	
	private RpsGame gameVals;
	public void start(Stage stage) {
		Group root = new Group(); 
		gameVals = new RpsGame();
		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);
		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 
		
		stage.show();
		
		VBox content = new VBox();
		HBox btnRow = new HBox();
		HBox textRow = new HBox();
		Button rockB = new Button("Rock");
		Button paperB = new Button("Paper");
		Button scissorsB = new Button("Scissors");
		TextField welcomeMsg = new TextField("Welcome!");
		TextField noWins = new TextField("wins: 0");
		TextField noLosses = new TextField("losses: 0");
		TextField noTies = new TextField("ties: 0");
		rockB.setOnAction(new RpsChoice(welcomeMsg, noWins, noLosses, noTies, "rock", gameVals));
		paperB.setOnAction(new RpsChoice(welcomeMsg, noWins, noLosses, noTies, "paper", gameVals));
		scissorsB.setOnAction(new RpsChoice(welcomeMsg, noWins, noLosses, noTies, "scissors", gameVals));
		welcomeMsg.setPrefWidth(400);
		btnRow.getChildren().addAll(rockB, paperB, scissorsB);
		textRow.getChildren().addAll(welcomeMsg, noWins, noLosses, noTies);
		content.getChildren().addAll(btnRow, textRow);
		root.getChildren().add(content);
	}
    public static void main(String[] args) {
        Application.launch(args);
    }
}
