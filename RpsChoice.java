// Raagav Prasanna 2036159
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class RpsChoice implements EventHandler<ActionEvent> {
    private TextField message;
    private TextField winsMsg;
    private TextField lossMsg;
    private TextField tiesMsg;
    private String choice;
    private RpsGame rpsObj;
    public RpsChoice(TextField message, TextField winsMsg, TextField lossMsg, TextField tiesMsg, String choice, RpsGame rpsObj) {
        this.message = message; 
        this.winsMsg = winsMsg;
        this.lossMsg = lossMsg;
        this.tiesMsg = tiesMsg;
        this.choice = choice;
        this.rpsObj = rpsObj;
    }
    @Override
    public void handle(ActionEvent e) {
        String winner = this.rpsObj.playRound(this.choice);
        this.message.setText(winner);
        winsMsg.setText("wins: " +this.rpsObj.getWins());
        lossMsg.setText("losses: " +this.rpsObj.getLoss());
        tiesMsg.setText("ties: " +this.rpsObj.getTies());
    }
}
